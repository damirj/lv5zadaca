package hr.ferit.damir.lv5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EnterBookActivity extends AppCompatActivity {

    EditText etAuthor, etTitle, etPageCount;
    Button bSaveToDB, bBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_book);
        setUpUI();
    }

    private void setUpUI(){
        etAuthor = (EditText) findViewById(R.id.etAuthor);
        etTitle = (EditText) findViewById(R.id.etTitle);
        etPageCount = (EditText) findViewById(R.id.etPageCount);

        bBack = (Button) findViewById(R.id.bBack);
        bSaveToDB = (Button) findViewById(R.id.bSaveToDB);
        this.bSaveToDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = etTitle.getText().toString();
                String author = etAuthor.getText().toString();
                String pageCount = etPageCount.getText().toString();
                int number = Integer.parseInt(pageCount);

                Book book = new Book(title, author,number);
                BookDBHelper.getInstance(getApplicationContext()).insertBook(book);
                Toast.makeText(getApplicationContext(), "Ubaceno u bazu", Toast.LENGTH_SHORT).show();
               // BookAdapter adapter = (BookAdapter) lvBookList.getAdapter();
                // adapter.insert(book);
            }
        });

        this.bBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent explicitIntent1 = new Intent();
                explicitIntent1.setClass(getApplicationContext(), MainActivity.class);
                startActivity (explicitIntent1);
            }
        });

    }
}
