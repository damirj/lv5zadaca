package hr.ferit.damir.lv5;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class BookAdapter extends BaseAdapter {
    private ArrayList<Book> mBooks;
    public BookAdapter(ArrayList<Book> books) { mBooks = books; }
    @Override
    public int getCount() { return this.mBooks.size(); }
    @Override
    public Object getItem(int position) { return this.mBooks.get(position); }
    @Override
    public long getItemId(int position) { return position; }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder bookViewHolder;
        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_book, parent, false);
            bookViewHolder = new ViewHolder(convertView);
            convertView.setTag(bookViewHolder);
        }
        else{
            bookViewHolder = (ViewHolder) convertView.getTag();
        }
        Book book = this.mBooks.get(position);
        bookViewHolder.tvBookTitle.setText(book.getTitle());
        bookViewHolder.tvBookAuthor.setText(book.getAuthor());
        bookViewHolder.tvBookPageCount.setText(String.valueOf(book.getPageCount()));
        return convertView;
    }
    public void insert(Book book) {
        this.mBooks.add(book);
        this.notifyDataSetChanged();
    }
    public static class ViewHolder {
        public TextView tvBookTitle, tvBookAuthor, tvBookPageCount;
        public ViewHolder(View bookView) {
            tvBookTitle = (TextView) bookView.findViewById(R.id.tvBookTitle);
            tvBookAuthor = (TextView) bookView.findViewById(R.id.tvBookAuthor);
            tvBookPageCount = (TextView) bookView.findViewById(R.id.tvPageCount);
        }
    }
}
