package hr.ferit.damir.lv5;

public class Book {
    private String title, author;
    private int pageCount;

    public Book(String title, String author, int pageCount){
        this.author = author;
        this.title = title;
        this.pageCount = pageCount;
    }

    public String getTitle(){
        return title;
    }


    public String getAuthor(){
        return author;
    }


    public int getPageCount(){
        return pageCount;
    }
}
