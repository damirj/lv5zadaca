package hr.ferit.damir.lv5;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button bAddBook;
    ListView lvBookList;
    FloatingActionButton fabAddBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpUI();
    }

    private void setUpUI(){
        bAddBook = (Button) findViewById(R.id.bAddBook);
        lvBookList = (ListView) findViewById(R.id.lvBookList);
        ArrayList<Book> books = this.loadBooks();
        BookAdapter bookAdapter = new BookAdapter(books);
        this.lvBookList.setAdapter(bookAdapter);

        this.bAddBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = new Book("El Classico", "CR7", 50);
                BookDBHelper.getInstance(getApplicationContext()).insertBook(book);

                BookAdapter adapter = (BookAdapter) lvBookList.getAdapter();
                adapter.insert(book);
            }
        });

        this.fabAddBook=(FloatingActionButton) this.findViewById(R.id.fabAddBook);
        this.fabAddBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), EnterBookActivity.class);
                startActivity (explicitIntent);
            }
        });

    }

    private ArrayList <Book> loadBooks(){
        return BookDBHelper.getInstance(this).getAllBooks();
    }
}
