package hr.ferit.damir.lv5;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class BookDBHelper extends SQLiteOpenHelper {
    // Singleton
    private static BookDBHelper mBookDBHelper = null;
    private BookDBHelper (Context context){
        super(context.getApplicationContext(),Schema.DATABASE_NAME,null,Schema.SCHEMA_VERSION);
    }
    public static synchronized BookDBHelper getInstance(Context context){
        if(mBookDBHelper == null){
            mBookDBHelper = new BookDBHelper(context);
        }
        return mBookDBHelper;
    }
    @Override
    public void onCreate(SQLiteDatabase db) { db.execSQL(CREATE_TABLE_MY_BOOKS); }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_MY_BOOKS);
        this.onCreate(db);
    }
    //SQL statements
    static final String CREATE_TABLE_MY_BOOKS = "CREATE TABLE " + Schema.TABLE_MY_BOOKS +
            " (" + Schema.AUTHOR + " TEXT," + Schema.TITLE + " TEXT," + Schema.PAGE_COUNT + " INTEGER);";
    static final String DROP_TABLE_MY_BOOKS = "DROP TABLE IF EXISTS " + Schema.TABLE_MY_BOOKS;
    static final String SELECT_ALL_BOOKS = "SELECT " + Schema.AUTHOR + "," + Schema.TITLE + ","
            +
            Schema.PAGE_COUNT + " FROM " + Schema.TABLE_MY_BOOKS;
    // CRUD should be performed on another thread
    public void insertBook(Book book){
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.AUTHOR, book.getAuthor());
        contentValues.put(Schema.TITLE, book.getTitle());
        contentValues.put(Schema.PAGE_COUNT, book.getPageCount());
        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        writeableDatabase.insert(Schema.TABLE_MY_BOOKS, Schema.AUTHOR,contentValues);
        writeableDatabase.close();
    }
    public ArrayList<Book> getAllBooks(){
        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        Cursor bookCursor = writeableDatabase.rawQuery(SELECT_ALL_BOOKS,null);
        ArrayList<Book> books = new ArrayList<>();
        if(bookCursor.moveToFirst()){
            do{
                String author = bookCursor.getString(0);
                String title = bookCursor.getString(1);
                int pageCount = bookCursor.getInt(2);
                books.add(new Book(author, title, pageCount));
            }while(bookCursor.moveToNext());
        }
        bookCursor.close();
        writeableDatabase.close();
        return books;
    }
    public static class Schema{
        private static final int SCHEMA_VERSION = 1;
        private static final String DATABASE_NAME = "books.db";
        //A table to store owned books:
        static final String TABLE_MY_BOOKS = "my_books";
        static final String AUTHOR = "author";
        static final String TITLE = "title";
        static final String PAGE_COUNT = "page_count";
    }
}
